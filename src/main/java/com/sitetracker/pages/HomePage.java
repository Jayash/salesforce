package com.sitetracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class HomePage {
	
	public WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		
	}
	
	By searchInput = By.id("st-search-input");

	By search = By.xpath("//div[@class='social-search clearfix pull-right']//button[@type='submit']");
	
	By linkWritingTests = By.xpath("//div[@id='st-results-container']//div[1]//a[contains(@href, 'apex_intro_writing_tests.htm#!')]");
	
	By linkTestingApex = By.xpath("//a[contains(@title, 'Apex provides a testing framework')]");
	
	By textWritingTests = By.xpath("//span[@class='ph'][contains(text(),'Writing Tests')]");
	
	public void search(String seachText) {
		driver.findElement(searchInput).sendKeys(seachText);
		driver.findElement(search).click();
	}
	
	public void gotoWritingTestPage() {
		driver.findElement(linkWritingTests).click();
		waitForElementPresent(textWritingTests);
	}
	
	public void waitForElementPresent(By by) {
 		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
}