package com.sitetracker.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WritingTestsPage {
	
	public WebDriver driver;
	
	public WritingTestsPage(WebDriver driver) {
		this.driver = driver;
		
	}

	
	By linkTestingApex = By.xpath("//a[contains(@title, 'Apex provides a testing framework')]");
	
	By textTestingApex = By.xpath("//span[@id='topic-title'][text()='Testing Apex']");
	
	public void gotoTestingApexPage() {
		driver.findElement(linkTestingApex).click();
		waitForElementPresent(textTestingApex);
	}
	
	public void waitForElementPresent(By by) {
 		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
}