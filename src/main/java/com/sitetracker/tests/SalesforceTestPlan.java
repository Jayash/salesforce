package com.sitetracker.tests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.sitetracker.pages.HomePage;
import com.sitetracker.pages.WritingTestsPage;


public class SalesforceTestPlan 
{
    public WebDriver driver;
    
    @Parameters("browser")
    @BeforeTest
    public void setup(String browser) {
    	
    	if (browser.equals("chrome")) {
	    	ChromeOptions Chrome_Profile = new ChromeOptions();
	    	Chrome_Profile.addArguments("disable-infobars"); 
	      	driver = new ChromeDriver(Chrome_Profile);
	    	System.setProperty("webdriver.chrome.driver", "../../chromedriver.exe");
    	} else if (browser.equals("ie")) {
	      	driver = new InternetExplorerDriver();
	      	System.setProperty("webdriver.ie.driver", "../../IEDriverServer.exe");
	    } else if (browser.equals("firefox")) {
	      	driver = new FirefoxDriver();
	      	System.setProperty("webdriver.gecko.driver", "../../geckodriver.exe");
		}
    	driver.manage().window().maximize();
    	driver.get("https://developer.salesforce.com/");
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    
    @Test
    public void search() {
    	HomePage homepage = new HomePage(driver);
    	homepage.search("Writing Tests");
    	homepage.gotoWritingTestPage();
    	String writingTestsTitle = "Writing Tests | Apex Developer Guide | Salesforce Developers";
    	Assert.assertEquals(driver.getTitle(), writingTestsTitle);
    	WritingTestsPage writingTests = new WritingTestsPage(driver);
    	writingTests.gotoTestingApexPage();
    	String apexTestsTitle = "Testing Apex | Apex Developer Guide | Salesforce Developers";
    	Assert.assertEquals(driver.getTitle(), apexTestsTitle);
    }
    
    @Test
    public void teardown() {
    	driver.quit();
    }

    	   
    	   
    	   
    	   
    	   
    	   
    	   
    	   
    	   

    	 
}
    
   

